﻿using System;
using System.Security.Principal;
using Elika.Framework.Desktop.Auth;

namespace Elika.Framework.Desktop
{
    public class ElikaIdentity : IIdentity
    {
        private readonly SessionInfo _sessionInfo;

        public ElikaIdentity(SessionInfo sessionInfo)
        {
            _sessionInfo = (SessionInfo) sessionInfo.Clone();
        }

        internal ElikaIdentity() { }

        public string AccessToken => _sessionInfo?.AccessToken;
        public string AuthenticationType { get; } = "Elika";
        public bool IsAuthenticated => _sessionInfo != null && !string.IsNullOrEmpty(_sessionInfo.UserName);
        public string Name => _sessionInfo?.UserName;
    }
}
