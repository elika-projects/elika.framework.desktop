﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elika.Framework.Desktop.Auth;
using Microsoft.Extensions.DependencyInjection;

namespace Elika.Framework.Desktop
{
    public class ElikaAppBuilder
    {
        private readonly IServiceCollection _services;

        internal ElikaAppBuilder(IServiceCollection services)
        {
            _services = services;
        }

        public ElikaAppBuilder UsePrincipalStore<TStore>() where TStore : class, IPrincipalStore, new()
        {
            _services.AddSingleton<IPrincipalStore, TStore>();
            return this;
        }
    }
}
