﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Elika.Framework.Desktop.Auth;

namespace Elika.Framework.Desktop
{
    public class ElikaPrincipal : IPrincipal
    {
        private readonly HashSet<string> _roles = new HashSet<string>();

        public ElikaPrincipal(SessionInfo sessionInfo)
        {
            _roles = new HashSet<string>(sessionInfo.Roles.Select(s => s.ToLower()));
            Identity = new ElikaIdentity(sessionInfo);
        }

        internal ElikaPrincipal()
        {
            Identity = new ElikaIdentity();
        }

        public bool IsInRole(string role)
        {
            if (role == null)
                throw new ArgumentNullException(nameof(role));

            if (role == string.Empty)
                throw new ArgumentException();

            return _roles.Contains(role);
        }

        public IIdentity Identity { get; }

        internal static ElikaPrincipal Empty { get; } = new ElikaPrincipal();
    }
}
