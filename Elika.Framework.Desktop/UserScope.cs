﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Elika.Framework.Desktop.Auth;
using Microsoft.Extensions.DependencyInjection;

namespace Elika.Framework.Desktop
{
    public class UserScope : IDisposable
    {
        internal UserScope(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public IDictionary<object, object> Properties = new ConcurrentDictionary<object, object>();

        public IServiceProvider ServiceProvider { get; }

        public IPrincipal Principal => ServiceProvider.GetRequiredService<IPrincipalStore>().GetPrincipal();

        public bool IsAuthenticated => Principal?.Identity?.IsAuthenticated ?? false;

        public ElikaApplication Application => ServiceProvider.GetRequiredService<ElikaApplication>();

        public void Authorize(string userName, string password)
        {
            var authService = ServiceProvider.GetRequiredService<AuthService>();

            if (Principal.Identity.IsAuthenticated)
                authService.Logout();

            authService.Authorize(userName, password);

            OnAuthorized?.Invoke(userName);
        }

        public void Logout()
        {
            var authService = ServiceProvider.GetRequiredService<AuthService>();
            authService.Logout();
            OnLogout?.Invoke();
        }

        internal static UserScope Create(IServiceCollection services)
        {
            services.AddSingleton(p => new UserScope(p))
                .AddTransient(p => p.GetRequiredService<UserScope>().Principal);

            var provider = services.BuildServiceProvider();

            return provider.GetRequiredService<UserScope>();
        }

        public delegate void DelOnAuthorized(string userName);

        public event DelOnAuthorized OnAuthorized;

        public delegate void DelOnLogout();

        public event DelOnLogout OnLogout;

        public void Dispose()
        {
            Logout();
        }
    }
}
