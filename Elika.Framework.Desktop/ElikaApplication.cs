﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Elika.DependencyInjection;
using Elika.Framework.Desktop.Auth;
using Microsoft.Extensions.DependencyInjection;

namespace Elika.Framework.Desktop
{
    public class ElikaApplication
    {
        internal ElikaApplication(IServiceCollection services)
        {
            _services = services;
        }

        public IDictionary<object, object> Properties = new ConcurrentDictionary<object, object>();

        private IServiceCollection _services { get; }
        
        public UserScope CreateUserScope() => UserScope.Create(_services);

        public static ElikaApplication SetUp<TDependencyModule>(Action<ElikaAppBuilder> builderFunc) where TDependencyModule : class, IDependencyModule, new()
        {
            return SetUp(builderFunc, services => services.AddModule<TDependencyModule>());
        }

        public static ElikaApplication SetUp(Action<ElikaAppBuilder> builderFunc, Action<IServiceCollection> serviceFactory = null)
        {
            var services = new ServiceCollection();
            services
                .AddScoped<AuthService>()
                .AddSingleton<IPrincipalStore, ScopePrincipalStore>();

            var builder = new ElikaAppBuilder(services);
            builderFunc?.Invoke(builder);
            serviceFactory?.Invoke(services);

            var app = new ElikaApplication(services);
            services.AddSingleton(app);

            return app;
        }
    }
}
