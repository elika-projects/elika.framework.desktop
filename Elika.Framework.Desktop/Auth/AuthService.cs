﻿using System;

namespace Elika.Framework.Desktop.Auth
{
    internal class AuthService
    {
        private readonly IUserStore _userStore;
        private readonly IPrincipalStore _principalStore;

        public AuthService(IUserStore userStore, IPrincipalStore principalStore)
        {
            _userStore = userStore;
            _principalStore = principalStore;
        }

        public void Authorize(string userName, string password)
        {
            var result = _userStore.Authorize(userName, password);

            if (!result.IsSuccess)
            {

            }

            var userData = result.Result;

            if (userData == null)
                throw new InvalidOperationException();

            var principal = new ElikaPrincipal(userData);
            _principalStore.SetPrincipal(principal);
        }

        public void Logout()
        {
            _principalStore.SetPrincipal(null);
        }
    }
}
