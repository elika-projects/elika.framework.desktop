﻿using System;
using System.Security.Principal;
using System.Threading;

namespace Elika.Framework.Desktop.Auth
{
    public class ScopePrincipalStore : IPrincipalStore
    {
        private IPrincipal _principal;

        public void SetPrincipal(IPrincipal principal)
        {
            _principal = principal;
        }

        public IPrincipal GetPrincipal()
        {
            return _principal ?? ElikaPrincipal.Empty;
        }
    }
}
