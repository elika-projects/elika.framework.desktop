﻿using System;
using System.Security.Principal;
using System.Threading;

namespace Elika.Framework.Desktop.Auth
{
    public class ThreadPrincipalStore : IPrincipalStore
    {
        public void SetPrincipal(IPrincipal principal)
        {
            AppDomain.CurrentDomain.SetThreadPrincipal(principal);
        }

        public IPrincipal GetPrincipal()
        {
            return Thread.CurrentPrincipal;
        }
    }
}
