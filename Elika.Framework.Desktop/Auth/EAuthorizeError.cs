﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elika.Framework.Desktop.Auth
{
    public enum EAuthorizeError
    {
        OK,
        UserNotFound,
        PassportIncorrect,
        UserBlocked,
        Error
    }
}
