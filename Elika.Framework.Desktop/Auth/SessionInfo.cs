﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elika.Framework.Desktop.Auth
{
    public class SessionInfo : UserData
    {
        public string AccessToken { get; set; }
    }
}
