﻿using System;
using System.Security.Principal;

namespace Elika.Framework.Desktop.Auth
{
    public interface IPrincipalStore
    {
        void SetPrincipal(IPrincipal principal);

        IPrincipal GetPrincipal();
    }
}
