﻿namespace Elika.Framework.Desktop.Auth
{
    public interface IUserStore
    {
        OperationResult<SessionInfo, EAuthorizeError> Authorize(string userName, string password);

        OperationResult<UserData> GetCurrentUser(string sessionKey);
    }
}
