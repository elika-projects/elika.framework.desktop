﻿using System;
using System.Collections.Generic;

namespace Elika.Framework.Desktop.Auth
{
    public class UserData : ICloneable
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string[] Roles { get; set; } = { };

        public Dictionary<string, string> ExtraData { get; set; } = new Dictionary<string, string>();
        public object Clone()
        {
            var roles = new string[Roles.Length];
            Roles.CopyTo(roles, 0);

            return new UserData
            {
                Id = Id,
                UserName = UserName,
                FirstName = FirstName,
                MiddleName = MiddleName,
                LastName = LastName,
                Roles = roles,
                ExtraData = new Dictionary<string, string>(ExtraData)
            };
        }
    }
}
