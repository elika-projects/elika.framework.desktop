using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Elika.Framework.Desktop.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var app = ElikaApplication.SetUp(builder => { });
            using (var userScope = app.CreateUserScope())
            {
                userScope.Authorize("123", "123");

                using (var scope = userScope.ServiceProvider.CreateScope())
                {
                    // ...
                }
            }
            Assert.Pass();
        }
    }
}